# Ryan's Docker-Compose Files

A collection of useful docker-compose files for quickly running instances of various frameworks, tools, and databases

## Getting Started

### Prerequisites

To run any of these files you will simply need Docker installed on your machine

## Run it

From the projects root directory, each compose file can be run by executing the following commands

### ElasticSearch + Kibana

```
docker-compose -f ./elastic/elastic-compose.yml up -d
```

### Flink

```
docker-compose -f ./flink/flink-compose.yml up -d
```

### Cassandra

```
docker-compose -f ./cassandra/cassandra-compose.yml up -d
```

